# Discussing SuperLiminal, 2023-01-17

with [[John Abbe]], [[Peter Kaminski]]

---

This is a public [[Massive Wiki]] ([[MassiveWiki]]?) for _______________

- "Liminal"
- between what and what else?
- four or five (patterns, stories, principles, rants, etc.) to give a sense of the scope

---

- pattern languages
    - same/similar patterns in different patterns help illuminate and bridge, in the same way a pattern language illuminates and bridges
- systems - incl story, held lightly ([[Donella Meadows]])
    - cf. "strong opinions, weakly held" ([[Paul Saffo]])
    - cf. "hold onto your needs tightly, strategies lightly" (NVC saying)
    - second order cybernetics

---

- quality that has no name
    - [[Quality Without A Name]] (QWAN)
- (healing, thrival, etc.) right relationship with ourselves, each other, and the rest of the natural world
- beloved community
- world that works for all
- all our relations
- resources to people and ecosystems

---

wiki way - wiki practice (wiki - a how) - quality that has no name
verb for pattern languaging - helping people think with patterns, and think and learn in patterns and in pattern languages.
collaborators
people you like
people you know
(others)

---

to do what?
ask the group that comes together
say what we're doing and how - Invitation
group exists and has a explicit or nascent purpose to be found / articulated
inter-group commonalities/complementarities exist and can be found / articulated

---

People we like, people we know, people who are activated right now

---

Current interests include:

'Next' technologies (that can be assumed, for example [[email, messaging, and the web]]) and 'next' use patterns with existing and upcoming technologies. Fediverse? On-boarding. Better software/interfaces.

Group-forming, inter-group forming, groupware - serving groups in their own processes and purposes, as well as in inter-group relations. There are many particular groups (and individuals) we see coming together, and/or see value in bringing together.

## Massive Wiki

- "email game massive wiki"
- streamlining onboarding for non-technical people