# Welcome to SuperLiminal

This is a public [[Massive Wiki]] ([[MassiveWiki]]?) for _______________

- "Liminal" - between what and what else?
- four or five (patterns, stories, principles, rants, etc.) to give a sense of the scope
	- becoming ecological
	- 
- related memology - solarpunk, regenerative, post-scarcity, post-capitalist
- from another context: "among all those seeking relief and/or engagement, justice, sovereignty, survival/resilience/regeneration/etc/thrival; and among people-centered/serving tech users/makers; and among those looking at process systemically (such as NGL, CII, many others), etc."
- I am a geek, from the Internet --Slow


Current interests include:

'Next' technologies (that can be assumed, for example [[email, messaging, and the web]]) and 'next' use patterns with existing and upcoming technologies. Fediverse? On-boarding. Better software/interfaces.

Group-forming, inter-group forming, groupware - serving groups in their own processes and purposes, as well as in inter-group relations. There are many particular groups (and individuals) we see coming together, and/or see value in bringing together.



== == ==

Massive Wiki treats README as a special case, because in git* "README.MD" is the index.html of a repo folder (all the way down). The top level one is critical for the whole project. MW uses a file named the same as the name of the directory.

Any (other) conventions for what goes in the README of a MassiveWiki? standard transcludable/copyable chunk?



someday transclude these:

MassiveWiki - [principles](https://massive.wiki/massive_wiki_manifesto) (reads like a minimum feature list)

HackMD for collaborative editing - then copied (back) to wiki.



Small Things Loosely Joined






# Obsidian (et al, or just that?)
git (command line), 

*link how-to here*
(already written down somewhere?:)
In preferences:
Turn off titles
Install/Enable Obsidian Git plugin



## Understanding the command palette
(second-to-top right)

git add - putting one change into staging

git commit - chunk of changes describably as a one-liner

get to understanding fetch / pre-resolve thing



[[WikiFeaturesMostWanted]]


plaintext - line endings?


# Markdown minimal viable product:

## wiki structure
double brackets make links

## document structure
hashes for headers (line of dashes/equals works under, but...) 
dashes (or stars which is used for bold) for bullets

bold, italics (stars, underscores)

[[chunking, naming, linking]]

In MassiveWiki (as with most wikis), the chunk is a page.
Would like paragraph-sized chunks, for transclusion. We know that is very complex, in general and particularly for Markdown, so, what MassiveWiki does is use headers - h1 to h6 - as chunks.

Specifically:
h1 - title of the page =~ file name of page


SEText style headers

---------------
WikiCase

[[WikiCase]]